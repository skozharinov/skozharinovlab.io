---
title: Почетна
description: Лична страница Сергеја Kожаринова
socials:
  - link: mailto:me@sergeykozharinov.com
    id: email
  - link: https://telegram.me/skozharinov
    id: telegram
  - link: https://matrix.to/#/@master:rix.serkozh.me
    id: matrix
  - link: https://lem.serkozh.me/u/master
    id: lemmy
  - link: https://linkedin.com/in/skozharinov
    id: linkedin
  - link: https://gitlab.com/skozharinov
    id: gitlab
  - link: https://www.last.fm/user/skozharinov
    id: lastfm
  - link: https://www.discogs.com/user/s.kozharinov
    id: discogs
  - link: https://kinorium.com/user/91232
    id: kinorium
about: |
  Здраво! Зовем се Сергеј, програмер сам.

  Имам 22 године, живим у Београду.

  Да би сазнао још нешто о мени,
  посети енглеску верзију ове странице,
  јер сам баш лењ да напишем пуну верзију.
---
