---
type: cv
title: CV
description: Short summary of a Sergey Kozharinov's career, qualifications, and education.
languages:
  - id: english
    level: advanced
  - id: russian
    level: native
  - id: serbian
    level: intermediate
socials:
  - link: mailto:me@sergeykozharinov.com
    text: me@sergeykozharinov.com
    id: email
  - link: https://telegram.me/sergeykozharinov
    text: "@sergeykozharinov"
    id: telegram
pdf_message: "To get a PDF of this page, use Print-to-PDF function of your web browser.
Best with the following settings: 
A4 paper size,
no margins,
headers and footers unchecked."
---

I am a software engineer with experience in the development of
computer vision systems with ML components, compilers, and desktop applications.
I have some background in database design and build automation.
