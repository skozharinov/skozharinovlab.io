---
type: cv
title: CV
description: Краткое описание карьеры, образования и профессиональных навыков Сергея Кожаринова
languages:
  - id: english
    level: advanced
  - id: russian
    level: native
  - id: serbian
    level: intermediate
socials:
  - link: mailto:me@sergeykozharinov.com
    text: me@sergeykozharinov.com
    id: email
  - link: https://telegram.me/sergeykozharinov
    text: "@sergeykozharinov"
    id: telegram
aliases: /cv/ru/
pdf_message: "Чтобы получить PDF этой страницы, используйте функцию \"Печать в PDF\" вашего браузера,
Наилучший результат можно получить со следующими настройками:
размер бумаги A4,
без полей,
колонтитулы отключены."
---

Разработчик с опытом разработки систем компьютерного зрения с ИИ компонентами,
компиляторов и десктопного ПО.
У меня также есть некоторый опыт проектирования баз данных и автоматизации сборки.
