---
title: Образование
weight: 3
---

{{<ul>}}
  {{<education
      org="Высшая школа экономики"
      icon="hse"
      website="https://www.hse.ru/"
      degree="Неоконченное высшее образование (бакалавриат)"
      programme="Прикладная математика и информатика"
      start_date="2020"
      end_date="2021">}}
{{</ul>}}
