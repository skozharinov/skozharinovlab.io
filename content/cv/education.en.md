---
title: Education
weight: 3
---

{{<ul>}}
  {{<education
      org="Higher School of Economics"
      icon="hse"
      website="https://www.hse.ru/en/"
      degree="Unfinished Bachelor's Degree"
      programme="Applied Mathematics and Information Science"
      start_date="2020"
      end_date="2021">}}
{{</ul>}}
