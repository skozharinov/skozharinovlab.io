---
title: Skills
weight: 2
---

{{<ul>}}
  {{<skill-badge "C++" cpp>}}
  {{<skill-badge "Python" python>}}
  {{<skill-badge "Rust" rust>}}
  {{<skill-badge "Qt" qt>}}
  {{<skill-badge "Boost" boost>}}
  {{<skill-badge "OpenCV" opencv>}}
  {{<skill-badge "OpenVINO" openvino>}}
  {{<skill-badge "Linux" linux>}}
  {{<skill-badge "Bash" bash>}}
  {{<skill-badge "CMake" cmake>}}
  {{<skill-badge "Git" git>}}
  {{<skill-badge "Docker" docker>}}
  {{<skill-badge "GitLab CI" gitlab>}}
  {{<skill-badge "PostgreSQL" postgresql>}}
  {{<skill-badge "OOP" object>}}
  {{<skill-badge "Algorithms and Data Structures" algorithm>}}
  {{<skill-badge "Computer Vision" vision>}}
{{</ul>}}
