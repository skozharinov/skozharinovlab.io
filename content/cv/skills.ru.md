---
title: Навыки
weight: 2
---

{{<ul>}}
  {{<skill-badge "C++" cpp>}}
  {{<skill-badge "Python" python>}}
  {{<skill-badge "Rust" rust>}}
  {{<skill-badge "Qt" qt>}}
  {{<skill-badge "Boost" boost>}}
  {{<skill-badge "OpenCV" opencv>}}
  {{<skill-badge "OpenVINO" openvino>}}
  {{<skill-badge "Linux" linux>}}
  {{<skill-badge "Bash" bash>}}
  {{<skill-badge "CMake" cmake>}}
  {{<skill-badge "Git" git>}}
  {{<skill-badge "Docker" docker>}}
  {{<skill-badge "GitLab CI" gitlab>}}
  {{<skill-badge "PostgreSQL" postgresql>}}
  {{<skill-badge "ООП" object>}}
  {{<skill-badge "Алгоритмы и структуры данных" algorithm>}}
  {{<skill-badge "Компьютерное зрение" vision>}}
{{</ul>}}
