---
title: Experience
weight: 1
---

{{<ul>}}
  {{<employment
      title="Software Engineer"
      company="Intelligent Security Systems"
      icon="iss"
      website="https://issivs.com/"
      start_date="April 2022"
      end_date="Now">}}
- Implemented support for a new platform in the software, tooling and CI.
- Performed major refactoring to improve module separation
  and significantly simplify integration of new neural network frameworks and platforms.
  {{</employment>}}
  {{<employment
      title="Assistant"
      company="Samsung R&D Institute Russia"
      icon="samsung"
      website="https://research.samsung.com/srr"
      start_date="October 2021"
      end_date="April 2022">}}
- Contributed to the improvement of the internal testing toolset by implementing new features.
- Performed minor code refactoring to maintain high standards of code quality
  in a compiler for mobile neural network accelerators (NPU).
  {{</employment>}}
{{</ul>}}
