---
title: Главная
description: Личная страница Сергея Кожаринова
socials:
  - link: mailto:me@sergeykozharinov.com
    id: email
  - link: https://telegram.me/skozharinov
    id: telegram
  - link: https://matrix.to/#/@master:rix.serkozh.me
    id: matrix
  - link: https://lem.serkozh.me/u/master
    id: lemmy
  - link: https://linkedin.com/in/skozharinov
    id: linkedin
  - link: https://gitlab.com/skozharinov
    id: gitlab
  - link: https://www.last.fm/user/skozharinov
    id: lastfm
  - link: https://www.discogs.com/user/s.kozharinov
    id: discogs
  - link: https://kinorium.com/user/91232
    id: kinorium
about: |
  Привет! Если ты ещё не догадался, меня зовут Сергей, я ~~погром~~программист.

  Мне 22 года, живу в Белграде (не путать с Белгородом).

  Если хотите узнать обо мне ещё что-то, загляните на англоязычную версию этой страницы
  --- я слишком ленивый, чтобы писать всё это несколько раз на разных языках.
---
