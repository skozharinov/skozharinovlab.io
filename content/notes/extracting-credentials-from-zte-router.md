---
title: A way to extract admin credentials from a ZTE router
slug: extracting-credentials-from-zte-router
date: 2023-11-19T18:00:00+0100
---

Don't mess with ISP provided equipment, it is not yours, and you are not naughty, right???

This was tested on a `ZXHN H298Q V7.0` at the time of writing, your mileage may vary.

## Dump the configuration

1. Log in with normal user credentials.
2. Go to "Management & Diagnosis" tab.
3. Select "System Management" page.
4. Choose "User Configuration Management" option.
5. Click "Backup Configuration" button. A file named `config.bin` should appear in your downloads.

## Analyze the configuration

1. Make sure you have Python 3 and pip installed.
2. Go to the [GitHub page](https://github.com/mkst/zte-config-utility) and download ZIP of the repository.
3. Extract the downloaded archive and open a terminal in the extracted directory.
4. Run `pip install --user .` to install the tool and its dependencies.
5. Run the following command to decode the configuration:

   ```sh
   python3 -m examples.decode "<PATH-TO-DOWNLOADS>/config.bin" "<PATH-TO-DOWNLOADS>/config.xml"
   ```

6. Open `config.xml` from your downloads directory in any text editor.
7. Find a table named `DevAuthInfo`.
8. In that table, you are interested in entries named `User` and `Pass`.
