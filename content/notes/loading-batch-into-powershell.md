---
title: How to load environment variables set by a batch script into PowerShell
slug: loading-batch-into-powershell
date: 2023-11-17T20:00:00+01:00
---

Add this snippet to your `$PROFILE`:

```pwsh
function LoadBat {
  param(
    [Parameter(Position = 0, Mandatory)]
    [string] $BatPath
  )

  & "$env:SystemRoot\system32\cmd.exe" /C "`"$BatPath`" >NUL & set" |
    Select-String '^([^=]*)=(.*)$' | ForEach-Object {
      $name = $_.Matches[0].Groups[1].Value;
      $value = $_.Matches[0].Groups[2].Value;
      Set-Item -Path env:$name -Value $value
    }
}
```

After restarting your shell, use it like that:

```pwsh
LoadBat "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2019\Professional\Common7\Tools\VsDevCmd.bat"
```
