---
title: Proper way to do Bluetooth pairing on Linux & Windows dualboot
slug: bluetooth-pairing-on-linux-windows-dualboot
date: 2023-11-13T21:00:00+01:00
---

## What's the problem?

When you pair a Bluetooth device to your computer, a unique set of pairing keys is generated in the process.
One is stored on the computer, associated with your device's MAC address.
Another is stored on the device, associated with you computer's MAC address.
Since the MAC address of your computer is defined by its hardware,
Linux and WIndows will share it.
Thus, when you re-pair the device to a second OS, the device replaces its key with a new one, making the old pairing unusable.

## How to fix that

We will first pair the device on Linux, then pair it on Windows, and finally copy computer's key from Windows to Linux.
Make sure to record your computer's and device's MAC addresses at some point, you'll need them in the process.

This note assumes a relatively modern Linux distribution (= not ancient, at the time of writing)
with `systemd` init system and `bluez` for managing Bluetooth, and Windows 10+.

1. Create all the necessary device configuration on both systems.
   1. As usual, pair your device on Linux.
   2. Do the same on Windows.

2. Get the keys from Windows.
   1. Download PsExec from [here](https://learn.microsoft.com/en-us/sysinternals/downloads/psexec).
      It is necessary to get the priveleges required to view Bluetooth pairing keys.
   2. Open registry editor from console like this: `psexec -s -i regedit.exe`.
   3. Descend into `HKEY_LOCAL_MACHINE\SYSTEM`.
   4. Descend into `CurrentControlSet` or, if it does not exist, into `ControlSet001`.
   5. Descend into `Services\BTHPORT\Parameters\Keys`.
   6. Descend into the key corresponding to your computer's MAC address.
   7. For convinience export this key.
   8. In the exported file you will find a hex value of the pairing key corresponding to your device's MAC address.

   Alternatively, you can perform the same operations from Linux using a CLI registry browser like [chntpw](https://pogostick.net/~pnh/ntpasswd/)
   on a registry file `.../Windows/System32/config/SYSTEM`.

3. Put the keys into Linux.
   1. Assume `root` privileges.
   2. Go to `/var/lib/bluetooth/`.
   3. Descend into the directory corresponding to your computer's MAC address.
   4. Descend into the directory corresponding to your device's MAC address.
   5. Open the `info` file for editing.
   6. Find `[LinkKey]` section and replace the `Key` attribute value with the key you fetched from Windows
      (beware of the formatting difference).
   7. Restart Bluetooth service: `systemctl restart bluetooth`.

4. Check that device is now paired with both Linux and Windows.

**For Bluetooth LE** the steps are a bit different:

- Instead of a single value for a pairing key, you will find a key with multiple values in Windows registry.
  Specifically, you want `LTK`, `KeyLength`, `ERand`, `EDIV`, `IRK` and `CSRK`.

- On Linux, you need to put these parameters in a unique and special way:

  - `LTK` goes into `Key` of the `[LongTermKey]` section (beware of the formatting difference here too).
  - `KeyLength` goes into `EncSize` of the `[LongTermKey]` section as a decimal.
  - `ERand` goes into `Rand` of the `[LongTermKey]` section, reversed as hex, and *then* converted into decimal.
  - `EDIV` goes into `EDiv` of the `[LongTermKey]` section as a decimal.
  - `IRK` goes into `Key` of the `[IdentityResolvingKey]` section (if `IRK` exists).
  - `CSRK` goes into `Key` of the `[LocalSignatureKey]` section (if `CSRK` exists).
  - Sections `[PeripheralLongTermKey]`, `[SlaveLongTermKey]`, and any other which look like `[SomethingSomethingKey]` go away.

- If it still does not work, I don't know what to do (yet). You can have a drink :)

## Sources

1. <https://unix.stackexchange.com/questions/255509/bluetooth-pairing-on-dual-boot-of-windows-linux-mint-ubuntu-stop-having-to-p>.
2. <https://console.systems/2014/09/how-to-pair-low-energy-le-bluetooth.html>.
