---
title: "How to bypass Paradox Launcher in Cities: Skylines and Cities: Skylines II"
slug: bypassing-launcher-in-cities-skylines
date: 2023-12-04T19:00:00+0100
---

This method is for Windows. It will persist through updates.

## Cities: Skylines

Set the Steam launch options for the game to:

```shell
Cities %command%
```

## Cities: Skylines II

Set the Steam launch options for the game to:

```shell
Cities2 %command%
```
