---
title: Home
description: Sergey Kozharinov's Personal Webpage
socials:
  - link: mailto:me@sergeykozharinov.com
    id: email
  - link: https://telegram.me/skozharinov
    id: telegram
  - link: https://matrix.to/#/@master:rix.serkozh.me
    id: matrix
  - link: https://lem.serkozh.me/u/master
    id: lemmy
  - link: https://linkedin.com/in/skozharinov
    id: linkedin
  - link: https://gitlab.com/skozharinov
    id: gitlab
  - link: https://www.last.fm/user/skozharinov
    id: lastfm
  - link: https://www.discogs.com/user/s.kozharinov
    id: discogs
  - link: https://kinorium.com/user/91232
    id: kinorium
about: |
  Hi there! My name is Sergey, and I am a 22-year-old software engineer living in Belgrade.
  Computers have always been a hobby and passion of mine,
  and I am glad to be able to turn that passion into my profession.
  I am also an open source enthusiast and believe in the freedom of software and the power of sharing knowledge.

  In my free time, I enjoy playing computer games.
  My favorite games are The Witcher 3: Wild Hunt, Cities: Skylines, and Euro Truck Simulator 2.
  I firmly believe in DRM-free gaming and support the idea of owning and being able to access your games indefinitely.

  Music is also a vital part of my life.
  I enjoy listening to many genres, mainly metal, rock, and alternative.
  If you'd like to see what I'm currently listening to, check out my Last.fm profile (link below).

  In terms of movies and TV shows, some of my all-time favorites include
  Hachi: A Dog's Tale, The Shawshank Redemption, The Silence of the Lambs, and The Green Mile.
  I also enjoy watching Black Mirror, House, M.D., and Chernobyl.
  If you're interested in seeing what I've watched and my ratings,
  feel free to check out my Kinorium profile (link below).

  Outside of my main passions, I am also interested in cybersecurity and rail transport.
---
