// Loosely based on https://stackoverflow.com/a/76795904/18292805

enum Scheme {
  ORIGINAL = "original",
  LIGHT = "light",
  DARK = "dark"
}

function getSystemScheme(): Scheme {
  return window.matchMedia("(prefers-color-scheme: dark)").matches ? Scheme.DARK : Scheme.LIGHT;
}

function getStoredScheme(): Scheme | null {
  return localStorage.getItem("scheme") as (Scheme | null);
}

function storeScheme(scheme: Scheme): void {
  localStorage.setItem("scheme", scheme as string);
}

function clearStoredScheme(): void {
  localStorage.removeItem("scheme");
}

function getPreferredScheme(): Scheme {
  const storedScheme = getStoredScheme();

  if (storedScheme !== null) {
    return storedScheme;
  }

  return Scheme.ORIGINAL;
}

function flipScheme(media: MediaList): void {
  const mediaText = media.mediaText;
  let newMediaText = mediaText;

  if (mediaText.includes("light")) newMediaText = mediaText.replace("light", "dark");
  if (mediaText.includes("dark")) newMediaText = mediaText.replace("dark", "light");

  media.mediaText = newMediaText;
}

function applyScheme(scheme: Scheme): void {
  const systemScheme = getSystemScheme();
  if (scheme === Scheme.ORIGINAL) scheme = systemScheme;

  for (let stylesheet of document.styleSheets) {
    for (let rule of stylesheet.cssRules) {
      if (!(rule instanceof CSSMediaRule)) continue;

      let media = rule.media;

      const mediaText = media.mediaText;
      if (!mediaText.includes("prefers-color-scheme")) continue;

      const dummyMediaTag = "original-prefers-color-scheme";

      const isToggled = mediaText.includes(dummyMediaTag);
      if (isToggled && scheme === systemScheme) {
        flipScheme(media);
        media.deleteMedium(dummyMediaTag);
      } else if (!isToggled && scheme != systemScheme) {
        flipScheme(media);
        media.appendMedium(dummyMediaTag);
      }
    }
  }
}

function getToggleButton(): HTMLElement {
  return document.getElementsByClassName("scheme-toggle")[0] as HTMLElement;
}

function setToggleButtonState(scheme: Scheme): void {
  let button = getToggleButton();

  let [lightIcon, darkIcon] = Array.from(button.children) as HTMLElement[];

  switch (scheme) {
    case Scheme.ORIGINAL:
      lightIcon.style.filter = "grayscale(100%)";
      darkIcon.style.filter = "grayscale(100%)";
      break;
    case Scheme.LIGHT:
      lightIcon.style.filter = "";
      darkIcon.style.filter = "grayscale(100%)";
      break;
    case Scheme.DARK:
      lightIcon.style.filter = "grayscale(100%)";
      darkIcon.style.filter = "";
      break;
  }

  button.classList.remove("no-js");
}

function toggleScheme() {
  const SCHEMES = [Scheme.ORIGINAL, Scheme.LIGHT, Scheme.DARK];
  const scheme = getPreferredScheme();

  const schemeIndex = SCHEMES.indexOf(scheme);
  const newSchemeIndex = (schemeIndex + 1) % SCHEMES.length;

  const newScheme = SCHEMES[newSchemeIndex];

  applyScheme(newScheme);
  setToggleButtonState(newScheme);

  if (newScheme === Scheme.ORIGINAL) {
    clearStoredScheme();
  } else {
    storeScheme(newScheme);
  }
}

(() => {
  applyScheme(getPreferredScheme());

  window.addEventListener("load", () => {
    getToggleButton().addEventListener("click", () => toggleScheme());
    setToggleButtonState(getPreferredScheme());
    applyScheme(getPreferredScheme());
  });

  window.matchMedia("(prefers-color-scheme: dark)").addEventListener("change", () => {
    applyScheme(getPreferredScheme());
  });
})()
